/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucionpractica1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Main {

    public static void main(String[] args) throws IOException {
        
        System.out.println("");
        System.out.println("Práctica 1: Práctica de repaso");
        System.out.println("");

        int seleccion = -1;

        do {

            System.out.println("");
            System.out.println("Seleccione una opción:");
            System.out.println("1- Cálculo divisores.");
            System.out.println("2- Rellenado esquina.");
            System.out.println("3- Medidor capicua.");
            System.out.println("0- Salir.");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            Ejercicios ejercicios = new Ejercicios();

            try {
                String lectura = br.readLine();
                seleccion = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                seleccion = -1;
            }

            switch (seleccion) {

                case 0:
                    System.out.println("Adios.");
                    break;
                case 1:
                    ejercicios.seleccionMenu1();
                    break;
                case 2:
                    ejercicios.seleccionMenu2();
                    break;
                case 3:
                    ejercicios.seleccionMenu3();
                    break;
                default:
                    System.out.println("Opción incorrecta.");
                    break;

            }

        } while (seleccion != 0);

    }

}
