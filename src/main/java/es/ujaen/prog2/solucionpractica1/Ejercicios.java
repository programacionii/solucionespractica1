/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucionpractica1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ejercicios {

    public void seleccionMenu1() throws IOException {
        System.out.println("Menú -> Cálculo de factores primos");
        System.out.println("Introduce el número (2-100):");

        int numero = 0;
        boolean lecturaCorrecta;

        do {

            lecturaCorrecta = true;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            try {
                String lectura = br.readLine();
                numero = Integer.parseInt(lectura);

                if (numero < 2 && numero > 100) {
                    System.out.println("El número debe de estar entre 2 y 100");
                    lecturaCorrecta = false;
                }

            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                System.out.println("Error en la entrada, vuelve a probar");
                lecturaCorrecta = false;
            }
        } while (!lecturaCorrecta);

        calculoDivisores(numero);

    }

    private void calculoDivisores(int numero) {

        //Para encontrar los divisores de X, se empieza comprobando desde el 2 hasta el X/2, 
        //si se encuentra un divisor, se divide X y se continua prebando desde 2
        ArrayList<Integer> divisores = new ArrayList();

        int x = numero;
        int divisorActual = 2;

        while (divisorActual < x ) {

            if (x % divisorActual == 0) {

                divisores.add(divisorActual);
                x = x / divisorActual;
                divisorActual = 2;

            } else {
                divisorActual++;
            }

        }

        divisores.add(x);

        System.out.println("Los divisores del número " + numero + " son :" + divisores);

    }

    public void seleccionMenu2() throws IOException {
        System.out.println("Menú -> Dibujo esquina");
        System.out.println("Introduce el lado del cuadrado");

        int lado = 0, esquina = 0;
        boolean lecturaCorrecta;

        do {

            lecturaCorrecta = true;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            try {
                String lectura = br.readLine();
                lado = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                System.out.println("Error en la entrada, vuelve a probar");
                lecturaCorrecta = false;
            }
        } while (!lecturaCorrecta);

        System.out.println("Introduce la esquina seleccionada (0-3)");
        do {

            lecturaCorrecta = true;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            try {
                String lectura = br.readLine();
                esquina = Integer.parseInt(lectura);

                if (esquina < 0 && esquina > 4) {
                    System.out.println("El número debe de estar entre 0 y 3");
                    lecturaCorrecta = false;
                }

            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                System.out.println("Error en la entrada, vuelve a probar");
                lecturaCorrecta = false;
            }
        } while (!lecturaCorrecta);

        switch (esquina) {

            case 0:
                dibujoEsquinaSupIzq(lado);
                break;
            case 1:
                dibujoEsquinaSupDer(lado);
                break;
            case 2:
                dibujoEsquinaInfDer(lado);
                break;
            case 3:
                dibujoEsquinaInfIzq(lado);
                break;

        }

    }

    private void dibujoEsquinaSupIzq(int lado) {

        System.out.println("");

        for (int i = 0; i < lado; i++) {
            for (int j = 0; j < lado; j++) {

                if (i + j < lado) {
                    System.out.print("#");
                } else {
                    System.out.print("·");
                }

            }
            System.out.println("");
        }

    }

    private void dibujoEsquinaSupDer(int lado) {

        System.out.println("");

        for (int i = 0; i < lado; i++) {
            for (int j = 0; j < lado; j++) {

                if (i <= j) {
                    System.out.print("#");
                } else {
                    System.out.print("·");
                }

            }
            System.out.println("");
        }

    }

    private void dibujoEsquinaInfDer(int lado) {
        System.out.println("");

        for (int i = 0; i < lado; i++) {
            for (int j = 0; j < lado; j++) {

                if (i + j >= lado - 1) {
                    System.out.print("#");
                } else {
                    System.out.print("·");
                }

            }
            System.out.println("");
        }
    }

    private void dibujoEsquinaInfIzq(int lado) {

        System.out.println("");

        for (int i = 0; i < lado; i++) {
            for (int j = 0; j < lado; j++) {

                if (i >= j) {
                    System.out.print("#");
                } else {
                    System.out.print("·");
                }

            }
            System.out.println("");
        }
    }

    public void seleccionMenu3() throws IOException {

        System.out.println("Menú -> Medidor capicúa");

        String palabra = "";
        boolean lecturaCorrecta;

        System.out.println("Introduce la palabra capicua");
        do {

            lecturaCorrecta = true;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            try {
                palabra = br.readLine();

            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                System.out.println("Error en la entrada, vuelve a probar");
                lecturaCorrecta = false;
            }
        } while (!lecturaCorrecta);

        medidorCapicua(palabra);

    }

    private void medidorCapicua(String palabra) {

        palabra = palabra.replaceAll(" ", "");
        
        char[] capicua = new char[palabra.length()];
        int puntuacion = 0;

        for (int i = 0; i < palabra.length() / 2; i++) {
                        
            if (palabra.charAt(i) == palabra.charAt((palabra.length() - 1) - i)) {
                capicua[i] = '#';
                capicua[(palabra.length() - 1) - i] = '#';
                puntuacion++;
            } else {
                capicua[i] = '·';
                capicua[(palabra.length() - 1) - i] = '·';
            }
        }

        //Si la palabra tiene tamaño impar
        if (palabra.length() % 2 != 0) {

            capicua[palabra.length() / 2] = '#';
            puntuacion++;

        }

        System.out.println("Puntuacion: " + puntuacion);
        System.out.println(palabra);
        System.out.println(capicua);

    }

}
